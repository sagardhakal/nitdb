﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace NepalIntermodal.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.Banner> Banners { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.Bidder> Bidders { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.Bill> Bills{ get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.Contact> Contacts { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.UseFulLink> UseFullinks{ get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.News> News { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.Publication> Publications { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.About> Abouts { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.BoardMember> BoardMembers { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.LegalDocument> LegalDocuments { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.LegalDocument_Type> LegalDocument_Type { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.Gallery> Galleries { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.ICD> ICDs { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.ICD_Type> ICD_Type { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.Officer_message> Officer_message { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.CurrentActivity> CurrentActivities { get; set; }
        public System.Data.Entity.DbSet<NepalIntermodal.Areas.Admin.Models.AboutViewModel> AboutViewModel { get; set; }


    }
}