﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NepalIntermodal.Areas.Admin.Models
{
    public class Intermodal
    {
    }

    public class Banner : RockMandatory
    {
        public int BannerId { get; set; }
        public string Headline { get; set; }
        public string BaseLine { get; set; }
        public string Link { get; set; }
        public string ImageUrl { get; set; }
    }

    public class Bidder : RockMandatory
    {
        public int BidderId { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public string RepresentativeName { get; set; }
        public string ContactNo { get; set; }
        public DateTime Regestrationdate { get; set; }
        public string VatNo { get; set; }
    }
    public class Bill : RockMandatory
    {
        public int BillId { get; set; }
        public string BillDescription { get; set; }
        public string Topic { get; set; }
        public string ExpenseTopic { get; set; }
        public string FromProcess { get; set; }
        public long PanNo { get; set; }
        public string PaidTo { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string Price { get; set; }
        public string Remarks { get; set; }
        public DateTime UploadTime { get; set; }
    }


    public class Contact : RockMandatory
    {
        public int ContactId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        public string PhoneNo { get; set; }
        [Required]
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
       
    }

    public class UseFulLink : RockMandatory
    {
        public int UseFulLinkId { get; set; }
        public string Title { get; set; }
        public string URL { get; set; }
    }

    public class News : RockMandatory
    {
        public int NewsId { get; set; }
        public string Title { get; set; }
        [AllowHtml] 
        public string Description { get; set; }
        [DataType(DataType.Date)]
        public DateTime Publishdate { get; set; }
        public string Reference { get; set; }
        public string LinkUrl { get; set; }

    }
    public class Publication : RockMandatory
    {
        public int PublicationId { get; set; }
        [Required]
        public string Title { get; set; }
        
        [DataType(DataType.Date)]
        public DateTime Publishdate { get; set; }
        public string LinkUrl { get; set; }
        public Publication_Type Publication_Type { get; set; }
    }

    public enum Publication_Type
    {
        Report, Smarika, Notice
    }

    public class About : RockMandatory
    {
        public int AboutId { get; set; }
        [AllowHtml] 
        public string Objective { get; set; }
        [AllowHtml] 
        public string Overview { get; set; }
        [AllowHtml] 
        public string Function { get; set; }
        [AllowHtml] 
        public string Salient_Feature { get; set; }
        [Display(Name="Organizational Structure")]
        public string ImageUrl { get; set; }

    }
    public class BoardMember : RockMandatory
    {
        public int BoardMemberId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string ImageUrl { get; set; }
        
    }


    public class LegalDocument : RockMandatory
    {
        public int LegalDocumentId { get; set; }
        public string Title { get; set; }
        public string LinkUrl { get; set; }
     public virtual LegalDocument_Type LegalDocument_Type { get; set; }
        public int LegalDocument_TypeId { get; set; }
        
    }

    public class LegalDocument_Type : RockMandatory
    {
        public int LegalDocument_TypeId { get; set; }
        public string Name { get; set; }
        
    }

   
    public class Gallery : RockMandatory
    {
        public int GalleryId { get; set; }
        public string Controller { get; set; }
        public string DataId { get; set; }
        public string Tag { get; set; }
        [AllowHtml] 
        public string Description { get; set; }
        public string Title { get; set; }
        public string ImagesUrl { get; set; }
        

    }
    public class ICD : RockMandatory
    {
        public int ICDId { get; set; }
        [AllowHtml]
        public string Description { get; set; }
        public string ImageUrl { get; set; }

    }
    

    public class ICD_Type : RockMandatory
    {
        public int ICD_TypeId { get; set; }
        public string Name { get; set; }
        [AllowHtml] 
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        
    }

     public class Officer_message : RockMandatory
       {
         public int Officer_messageId { get; set; }
         public string Name { get; set; }
         public string Designation { get; set; }
         [AllowHtml] 
         public string Description { get; set; }
         public string ImageUrl { get; set; }
       }

  
    public class CurrentActivity : RockMandatory
    {
        public int CurrentActivityId { get; set; }
        public string Name { get; set; }
        [AllowHtml] 
        public string Description { get; set; }
    }

    public class AboutViewModel
    {
        public int AboutViewModelId { get; set; }
       [AllowHtml] 
        public string Description { get; set; }
        

    }
    public class ListViewModel
    {
        public int ListViewModelId { get; set; }
       
        public string Title { get; set; }
        public string LinkUrl { get; set; }



    }
    
   
    public class RockMandatory
    {
        [ScaffoldColumn(false)]
        [DataType(DataType.Date)]
        public DateTime? CreatedDate { get; set; }
        [ScaffoldColumn(false)]
        public string CreatedBy { get; set; }
        [DataType(DataType.Date)]
        [ScaffoldColumn(false)]
        public DateTime? ModifiedDate { get; set; }
        [ScaffoldColumn(false)]
        public string ModifiedBy { get; set; }
        [ScaffoldColumn(false)]
        public Boolean DelFlg { get; set; }
        public Boolean Active { get; set; }
    }
}