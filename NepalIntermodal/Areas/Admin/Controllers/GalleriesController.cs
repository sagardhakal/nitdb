﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class GalleriesController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Galleries
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingGalleryId = String.IsNullOrEmpty(sortingOrder) ? "GalleryId" : "";
				            												ViewBag.SortingController = String.IsNullOrEmpty(sortingOrder) ? "Controller" : "";
				            												ViewBag.SortingDataId = String.IsNullOrEmpty(sortingOrder) ? "DataId" : "";
				            												ViewBag.SortingTag = String.IsNullOrEmpty(sortingOrder) ? "Tag" : "";
				            												ViewBag.SortingDescription = String.IsNullOrEmpty(sortingOrder) ? "Description" : "";
													            												ViewBag.SortingTitle = String.IsNullOrEmpty(sortingOrder) ? "Title" : "";
				            												ViewBag.SortingImagesUrl = String.IsNullOrEmpty(sortingOrder) ? "ImagesUrl" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.Galleries select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            										
														
															item.Controller.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.DataId.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Tag.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Description.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Title.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.ImagesUrl.ToUpper().Contains(searchData.ToUpper()) );
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "GalleryId":
											items = items.OrderByDescending(item => item.GalleryId);
                    					break;
            			case "Controller":
											items = items.OrderByDescending(item => item.Controller);
                    					break;
            			case "DataId":
											items = items.OrderByDescending(item => item.DataId);
                    					break;
            			case "Tag":
											items = items.OrderByDescending(item => item.Tag);
                    					break;
            			case "Description":
											items = items.OrderByDescending(item => item.Description);
                    					break;
            			case "Title":
											items = items.OrderByDescending(item => item.Title);
                    					break;
            			case "ImagesUrl":
											items = items.OrderByDescending(item => item.ImagesUrl);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.GalleryId);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/Galleries/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Gallery gallery = db.Galleries.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var gallerya = from item in db.Galleries where (item.GalleryId == id) select item;
            gallerya = gallerya.Include(a => a.CreatedBy);
            gallerya = gallerya.Where(a => a.CreatedBy == userid);
            if (gallerya.Count() == 0)
            {
                gallery = null;
            }





            if (gallery == null)
            {
                return HttpNotFound();
            }
            return View(gallery);
        }

        // GET: Admin/Galleries/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Galleries/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
        			public ActionResult Create([Bind(Include = "GalleryId,Controller,DataId,Tag,Description,Title,ImagesUrl,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] Gallery gallery)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(gallery).State = EntityState.Added;
					gallery.CreatedBy = User.Identity.GetUserId();
                    gallery.CreatedDate = DateTime.Now;
        			    
				db.Galleries.Add(gallery);
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(gallery);
        }

        // GET: Admin/Galleries/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gallery gallery = db.Galleries.Find(id);
			var userid = User.Identity.GetUserId();
            var gallerya = from item in db.Galleries where (item.GalleryId == id) select item;
            gallerya = gallerya.Include(a => a.CreatedBy);
            gallerya = gallerya.Where(a => a.CreatedBy == userid);
            if (gallerya.Count() == 0)
            {
                gallery = null;
            }
            if (gallery == null)
            {
                return HttpNotFound();
            }
            return View(gallery);
        }

        // POST: Admin/Galleries/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
					public ActionResult Edit([Bind(Include = "GalleryId,Controller,DataId,Tag,Description,Title,ImagesUrl,CreatedDate,CreatedBy,DelFlg,Active")] Gallery gallery)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(gallery).State = EntityState.Modified;
				
				gallery.ModifiedBy = User.Identity.GetUserId();
                    gallery.ModifiedDate = DateTime.Now;
					                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(gallery);
        }

        // GET: Admin/Galleries/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Gallery gallery = db.Galleries.Find(id);
			var userid = User.Identity.GetUserId();
            var gallerya = from item in db.Galleries where (item.GalleryId == id) select item;
            gallerya = gallerya.Include(a => a.CreatedBy);
            gallerya = gallerya.Where(a => a.CreatedBy == userid);
            if (gallerya.Count() == 0)
            {
                gallery = null;
            }

            if (gallery == null)
            {
                return HttpNotFound();
            }
            return View(gallery);
        }

        // POST: Admin/Galleries/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Gallery gallery = db.Galleries.Find(id);
            db.Galleries.Remove(gallery);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
