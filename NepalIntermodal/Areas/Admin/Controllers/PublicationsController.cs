﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class PublicationsController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Publications
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingTitle = String.IsNullOrEmpty(sortingOrder) ? "Title" : "";
				            												ViewBag.SortingDescription = String.IsNullOrEmpty(sortingOrder) ? "Description" : "";
													            												ViewBag.SortingPublishdate = String.IsNullOrEmpty(sortingOrder) ? "Publishdate" : "";
				            																					ViewBag.SortingLinkUrl = String.IsNullOrEmpty(sortingOrder) ? "LinkUrl" : "";
				            												ViewBag.SortingPublication_Type = String.IsNullOrEmpty(sortingOrder) ? "Publication_Type" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.Publications select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            										
															item.Title.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															
													
																				
													
															item.LinkUrl.ToUpper().Contains(searchData.ToUpper()) );
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "Title":
											items = items.OrderByDescending(item => item.Title);
                    					break;
            			
            			case "Publishdate":
											items = items.OrderBy(item => item.Publishdate);	
										break;
            			case "LinkUrl":
											items = items.OrderByDescending(item => item.LinkUrl);
                    					break;
            			case "Publication_Type":
											items = items.OrderByDescending(item => item.Publication_Type);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.Title);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/Publications/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Publication publication = db.Publications.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var publicationa = from item in db.Publications where (item.PublicationId == id) select item;
            publicationa = publicationa.Include(a => a.CreatedBy);
            publicationa = publicationa.Where(a => a.CreatedBy == userid);
            if (publicationa.Count() == 0)
            {
                publication = null;
            }





            if (publication == null)
            {
                return HttpNotFound();
            }
            return View(publication);
        }

        // GET: Admin/Publications/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Publications/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
        			public ActionResult Create([Bind(Include = "PublicationId,Title,Publishdate,LinkUrl,Publication_Type,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] Publication publication, HttpPostedFileBase LinkUrl)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(publication).State = EntityState.Added;
					publication.CreatedBy = User.Identity.GetUserId();
                    publication.CreatedDate = DateTime.Now;
        			    
				db.Publications.Add(publication);
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(publication);
        }

        // GET: Admin/Publications/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Publication publication = db.Publications.Find(id);
			var userid = User.Identity.GetUserId();
            var publicationa = from item in db.Publications where (item.PublicationId == id) select item;
            publicationa = publicationa.Include(a => a.CreatedBy);
            publicationa = publicationa.Where(a => a.CreatedBy == userid);
            if (publicationa.Count() == 0)
            {
                publication = null;
            }
            if (publication == null)
            {
                return HttpNotFound();
            }
            return View(publication);
        }

        // POST: Admin/Publications/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
				public ActionResult Edit([Bind(Include = "PublicationId,Title,Publishdate,LinkUrl,Publication_Type,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] Publication publication, HttpPostedFileBase LinkUrl)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(publication).State = EntityState.Modified;
				
				publication.ModifiedBy = User.Identity.GetUserId();
                    publication.ModifiedDate = DateTime.Now;
					                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(publication);
        }

        // GET: Admin/Publications/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Publication publication = db.Publications.Find(id);
			var userid = User.Identity.GetUserId();
            var publicationa = from item in db.Publications where (item.PublicationId == id) select item;
            publicationa = publicationa.Include(a => a.CreatedBy);
            publicationa = publicationa.Where(a => a.CreatedBy == userid);
            if (publicationa.Count() == 0)
            {
                publication = null;
            }

            if (publication == null)
            {
                return HttpNotFound();
            }
            return View(publication);
        }

        // POST: Admin/Publications/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Publication publication = db.Publications.Find(id);
            db.Publications.Remove(publication);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
