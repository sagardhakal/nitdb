﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class CurrentActivitiesController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/CurrentActivities
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingCurrentActivityId = String.IsNullOrEmpty(sortingOrder) ? "CurrentActivityId" : "";
				            												ViewBag.SortingName = String.IsNullOrEmpty(sortingOrder) ? "Name" : "";
				            												ViewBag.SortingDescription = String.IsNullOrEmpty(sortingOrder) ? "Description" : "";
													            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.CurrentActivities select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            										
															
															item.Name.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Description.ToUpper().Contains(searchData.ToUpper()));
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "CurrentActivityId":
											items = items.OrderByDescending(item => item.CurrentActivityId);
                    					break;
            			case "Name":
											items = items.OrderByDescending(item => item.Name);
                    					break;
            			case "Description":
											items = items.OrderByDescending(item => item.Description);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.CurrentActivityId);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/CurrentActivities/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           CurrentActivity currentActivity = db.CurrentActivities.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var currentActivitya = from item in db.CurrentActivities where (item.CurrentActivityId == id) select item;
            currentActivitya = currentActivitya.Include(a => a.CreatedBy);
            currentActivitya = currentActivitya.Where(a => a.CreatedBy == userid);
            if (currentActivitya.Count() == 0)
            {
                currentActivity = null;
            }





            if (currentActivity == null)
            {
                return HttpNotFound();
            }
            return View(currentActivity);
        }

        // GET: Admin/CurrentActivities/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/CurrentActivities/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
        			public ActionResult Create([Bind(Include = "CurrentActivityId,Name,Description,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] CurrentActivity currentActivity)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(currentActivity).State = EntityState.Added;
					currentActivity.CreatedBy = User.Identity.GetUserId();
                    currentActivity.CreatedDate = DateTime.Now;
        			    
				db.CurrentActivities.Add(currentActivity);
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(currentActivity);
        }

        // GET: Admin/CurrentActivities/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CurrentActivity currentActivity = db.CurrentActivities.Find(id);
			var userid = User.Identity.GetUserId();
            var currentActivitya = from item in db.CurrentActivities where (item.CurrentActivityId == id) select item;
            currentActivitya = currentActivitya.Include(a => a.CreatedBy);
            currentActivitya = currentActivitya.Where(a => a.CreatedBy == userid);
            if (currentActivitya.Count() == 0)
            {
                currentActivity = null;
            }
            if (currentActivity == null)
            {
                return HttpNotFound();
            }
            return View(currentActivity);
        }

        // POST: Admin/CurrentActivities/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
					public ActionResult Edit([Bind(Include = "CurrentActivityId,Name,Description,CreatedDate,CreatedBy,DelFlg,Active")] CurrentActivity currentActivity)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(currentActivity).State = EntityState.Modified;
				
				currentActivity.ModifiedBy = User.Identity.GetUserId();
                    currentActivity.ModifiedDate = DateTime.Now;
					                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(currentActivity);
        }

        // GET: Admin/CurrentActivities/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CurrentActivity currentActivity = db.CurrentActivities.Find(id);
			var userid = User.Identity.GetUserId();
            var currentActivitya = from item in db.CurrentActivities where (item.CurrentActivityId == id) select item;
            currentActivitya = currentActivitya.Include(a => a.CreatedBy);
            currentActivitya = currentActivitya.Where(a => a.CreatedBy == userid);
            if (currentActivitya.Count() == 0)
            {
                currentActivity = null;
            }

            if (currentActivity == null)
            {
                return HttpNotFound();
            }
            return View(currentActivity);
        }

        // POST: Admin/CurrentActivities/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CurrentActivity currentActivity = db.CurrentActivities.Find(id);
            db.CurrentActivities.Remove(currentActivity);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
