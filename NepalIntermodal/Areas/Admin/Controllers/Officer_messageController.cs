﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class Officer_messageController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Officer_message
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingOfficer_messageId = String.IsNullOrEmpty(sortingOrder) ? "Officer_messageId" : "";
				            												ViewBag.SortingName = String.IsNullOrEmpty(sortingOrder) ? "Name" : "";
				            												ViewBag.SortingDesignation = String.IsNullOrEmpty(sortingOrder) ? "Designation" : "";
				            												ViewBag.SortingDescription = String.IsNullOrEmpty(sortingOrder) ? "Description" : "";
													            																					ViewBag.SortingImageUrl = String.IsNullOrEmpty(sortingOrder) ? "ImageUrl" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.Officer_message select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            													
													
															item.Name.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Designation.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Description.ToUpper().Contains(searchData.ToUpper()) );
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "Officer_messageId":
											items = items.OrderByDescending(item => item.Officer_messageId);
                    					break;
            			case "Name":
											items = items.OrderByDescending(item => item.Name);
                    					break;
            			case "Designation":
											items = items.OrderByDescending(item => item.Designation);
                    					break;
            			case "Description":
											items = items.OrderByDescending(item => item.Description);
                    					break;
            			case "ImageUrl":
											items = items.OrderByDescending(item => item.ImageUrl);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.Officer_messageId);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/Officer_message/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Officer_message officer_message = db.Officer_message.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var officer_messagea = from item in db.Officer_message where (item.Officer_messageId == id) select item;
            officer_messagea = officer_messagea.Include(a => a.CreatedBy);
            officer_messagea = officer_messagea.Where(a => a.CreatedBy == userid);
            if (officer_messagea.Count() == 0)
            {
                officer_message = null;
            }





            if (officer_message == null)
            {
                return HttpNotFound();
            }
            return View(officer_message);
        }

        // GET: Admin/Officer_message/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Officer_message/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
        			public ActionResult Create([Bind(Include = "Officer_messageId,Name,Designation,Description,ImageUrl,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] Officer_message officer_message, HttpPostedFileBase ImageUrl)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(officer_message).State = EntityState.Added;
					officer_message.CreatedBy = User.Identity.GetUserId();
                    officer_message.CreatedDate = DateTime.Now;
               
			 if (ImageUrl != null)
                {
                    string pathToCreate = "~/Images/officer_message";
                    if (!Directory.Exists(Server.MapPath(pathToCreate)))
                    {
                        //Now you know it is ok, create it
                        Directory.CreateDirectory(Server.MapPath(pathToCreate));
                    }
                    string extension = Path.GetExtension(ImageUrl.FileName);
                    string fileName = Path.GetFileNameWithoutExtension(ImageUrl.FileName);
                    string id = Guid.NewGuid().ToString();
                    string imageUrl = "/Images/officer_message/" + fileName + "" + /*main.MainId.ToString()*/ id + extension;

                    ImageUrl.SaveAs(Path.Combine(Server.MapPath(pathToCreate), fileName + "" + /*main.MainId.ToString()*/id + extension));
                    string DestinationPath = Path.Combine(Server.MapPath(pathToCreate));
                    DestinationPath += "\\" + fileName + id ;
					ImageResizer.ImageJob i = new ImageResizer.ImageJob(DestinationPath + extension , DestinationPath + "_thumb.jpg" , new ImageResizer.ResizeSettings(
                 "width=200;height=200;format=jpg;mode=max"));
					i.Build();
					
					officer_message.ImageUrl = imageUrl;
	                    
						db.Officer_message.Add(officer_message);

                }
                else
                {
					db.Officer_message.Add(officer_message);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(officer_message);
        }

        // GET: Admin/Officer_message/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Officer_message officer_message = db.Officer_message.Find(id);
			var userid = User.Identity.GetUserId();
            var officer_messagea = from item in db.Officer_message where (item.Officer_messageId == id) select item;
            officer_messagea = officer_messagea.Include(a => a.CreatedBy);
            officer_messagea = officer_messagea.Where(a => a.CreatedBy == userid);
            if (officer_messagea.Count() == 0)
            {
                officer_message = null;
            }
            if (officer_message == null)
            {
                return HttpNotFound();
            }
            return View(officer_message);
        }

        // POST: Admin/Officer_message/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
					public ActionResult Edit([Bind(Include = "Officer_messageId,Name,Designation,Description,ImageUrl,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] Officer_message officer_message,HttpPostedFileBase ImageUrl)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(officer_message).State = EntityState.Modified;
				
				officer_message.ModifiedBy = User.Identity.GetUserId();
                    officer_message.ModifiedDate = DateTime.Now;
				       
				if (ImageUrl != null)
                {
                    string pathToCreate = "~/Images/officer_message";
                    if (!Directory.Exists(Server.MapPath(pathToCreate)))
                    {
                        //Now you know it is ok, create it
                        Directory.CreateDirectory(Server.MapPath(pathToCreate));
                    }
                    string extension = Path.GetExtension(ImageUrl.FileName);
                    string fileName = Path.GetFileNameWithoutExtension(ImageUrl.FileName);
                    string id = Guid.NewGuid().ToString();
                    string imageUrl = "/Images/officer_message/" + fileName + "" + /*main.MainId.ToString()*/ id + extension;

                    ImageUrl.SaveAs(Path.Combine(Server.MapPath(pathToCreate), fileName + "" + /*main.MainId.ToString()*/id + extension));
					string DestinationPath = Path.Combine(Server.MapPath(pathToCreate));
                    DestinationPath += "\\" + fileName + id ;
					ImageResizer.ImageJob i = new ImageResizer.ImageJob(DestinationPath + extension , DestinationPath + "_thumb.jpg" , new ImageResizer.ResizeSettings(
					"width=200;height=200;format=jpg;mode=max"));
					i.Build();
					officer_message.ImageUrl = imageUrl;
                }
				                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(officer_message);
        }

        // GET: Admin/Officer_message/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Officer_message officer_message = db.Officer_message.Find(id);
			var userid = User.Identity.GetUserId();
            var officer_messagea = from item in db.Officer_message where (item.Officer_messageId == id) select item;
            officer_messagea = officer_messagea.Include(a => a.CreatedBy);
            officer_messagea = officer_messagea.Where(a => a.CreatedBy == userid);
            if (officer_messagea.Count() == 0)
            {
                officer_message = null;
            }

            if (officer_message == null)
            {
                return HttpNotFound();
            }
            return View(officer_message);
        }

        // POST: Admin/Officer_message/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Officer_message officer_message = db.Officer_message.Find(id);
            db.Officer_message.Remove(officer_message);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
