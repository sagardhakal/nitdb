﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class LegalDocumentsController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/LegalDocuments
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            var legalDocuments = db.LegalDocuments.Include(l => l.LegalDocument_Type);
            
			
			
			ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingTitle = String.IsNullOrEmpty(sortingOrder) ? "Title" : "";
				            																					ViewBag.SortingLinkUrl = String.IsNullOrEmpty(sortingOrder) ? "LinkUrl" : "";
				            												ViewBag.SortingDescription = String.IsNullOrEmpty(sortingOrder) ? "Description" : "";
													            												ViewBag.SortingLegalDocument_TypeId = String.IsNullOrEmpty(sortingOrder) ? "LegalDocument_TypeId" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			legalDocuments = from item in db.LegalDocuments select item;
			// Below Line for Community or else omit above line
			
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 legalDocuments =
                legalDocuments.Where(
                    item =>
            																

																			item.Title.ToUpper().Contains(searchData.ToUpper()) ||
																																			

																			item.LinkUrl.ToUpper().Contains(searchData.ToUpper())  );

																									
					}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "Title":
											legalDocuments = legalDocuments.OrderByDescending(item => item.Title);
                    					break;
            			case "LinkUrl":
											legalDocuments = legalDocuments.OrderByDescending(item => item.LinkUrl);
                    					break;
            			
            			case "LegalDocument_TypeId":
											legalDocuments = legalDocuments.OrderByDescending(item => item.LegalDocument_TypeId);
                    					break;
            			case "Active":
											legalDocuments = legalDocuments.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    legalDocuments = legalDocuments.OrderBy(item => item.Title);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(legalDocuments.ToPagedList(noOfPage ,totalPageSize ));
			//return View(legalDocuments.ToList());






        }

        // GET: Admin/LegalDocuments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           LegalDocument legalDocument = db.LegalDocuments.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var legalDocumenta = from item in db.LegalDocuments where (item.LegalDocumentId == id) select item;
            legalDocumenta = legalDocumenta.Include(a => a.CreatedBy);
            legalDocumenta = legalDocumenta.Where(a => a.CreatedBy == userid);
            if (legalDocumenta.Count() == 0)
            {
                legalDocument = null;
            }





            if (legalDocument == null)
            {
                return HttpNotFound();
            }
            return View(legalDocument);
        }

        // GET: Admin/LegalDocuments/Create
        public ActionResult Create()
        {
            ViewBag.LegalDocument_TypeId = new SelectList(db.LegalDocument_Type, "LegalDocument_TypeId", "Name");
            return View();
        }

        // POST: Admin/LegalDocuments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
        			public ActionResult Create([Bind(Include = "LegalDocumentId,Title,LinkUrl,LegalDocument_TypeId,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] LegalDocument legalDocument, HttpPostedFileBase LinkUrl)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(legalDocument).State = EntityState.Added;
					legalDocument.CreatedBy = User.Identity.GetUserId();
                    legalDocument.CreatedDate = DateTime.Now;
        			    
				db.LegalDocuments.Add(legalDocument);
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.LegalDocument_TypeId = new SelectList(db.LegalDocument_Type, "LegalDocument_TypeId", "Name", legalDocument.LegalDocument_TypeId);
            return View(legalDocument);
        }

        // GET: Admin/LegalDocuments/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LegalDocument legalDocument = db.LegalDocuments.Find(id);
			var userid = User.Identity.GetUserId();
            var legalDocumenta = from item in db.LegalDocuments where (item.LegalDocumentId == id) select item;
            legalDocumenta = legalDocumenta.Include(a => a.CreatedBy);
            legalDocumenta = legalDocumenta.Where(a => a.CreatedBy == userid);
            if (legalDocumenta.Count() == 0)
            {
                legalDocument = null;
            }
            if (legalDocument == null)
            {
                return HttpNotFound();
            }
            ViewBag.LegalDocument_TypeId = new SelectList(db.LegalDocument_Type, "LegalDocument_TypeId", "Name", legalDocument.LegalDocument_TypeId);
            return View(legalDocument);
        }

        // POST: Admin/LegalDocuments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
				public ActionResult Edit([Bind(Include = "LegalDocumentId,Title,LinkUrl,LegalDocument_TypeId,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] LegalDocument legalDocument, HttpPostedFileBase LinkUrl)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(legalDocument).State = EntityState.Modified;
				
				legalDocument.ModifiedBy = User.Identity.GetUserId();
                    legalDocument.ModifiedDate = DateTime.Now;
					                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.LegalDocument_TypeId = new SelectList(db.LegalDocument_Type, "LegalDocument_TypeId", "Name", legalDocument.LegalDocument_TypeId);
            return View(legalDocument);
        }

        // GET: Admin/LegalDocuments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LegalDocument legalDocument = db.LegalDocuments.Find(id);
			var userid = User.Identity.GetUserId();
            var legalDocumenta = from item in db.LegalDocuments where (item.LegalDocumentId == id) select item;
            legalDocumenta = legalDocumenta.Include(a => a.CreatedBy);
            legalDocumenta = legalDocumenta.Where(a => a.CreatedBy == userid);
            if (legalDocumenta.Count() == 0)
            {
                legalDocument = null;
            }

            if (legalDocument == null)
            {
                return HttpNotFound();
            }
            return View(legalDocument);
        }

        // POST: Admin/LegalDocuments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LegalDocument legalDocument = db.LegalDocuments.Find(id);
            db.LegalDocuments.Remove(legalDocument);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
