﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class UseFulLinksController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/UseFulLinks
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingUseFulLinkId = String.IsNullOrEmpty(sortingOrder) ? "UseFulLinkId" : "";
				            												ViewBag.SortingTitle = String.IsNullOrEmpty(sortingOrder) ? "Title" : "";
				            												ViewBag.SortingURL = String.IsNullOrEmpty(sortingOrder) ? "URL" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.UseFullinks select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            												
													
															item.Title.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.URL.ToUpper().Contains(searchData.ToUpper()) );
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "UseFulLinkId":
											items = items.OrderByDescending(item => item.UseFulLinkId);
                    					break;
            			case "Title":
											items = items.OrderByDescending(item => item.Title);
                    					break;
            			case "URL":
											items = items.OrderByDescending(item => item.URL);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.UseFulLinkId);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/UseFulLinks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           UseFulLink useFulLink = db.UseFullinks.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var useFulLinka = from item in db.UseFullinks where (item.UseFulLinkId == id) select item;
            useFulLinka = useFulLinka.Include(a => a.CreatedBy);
            useFulLinka = useFulLinka.Where(a => a.CreatedBy == userid);
            if (useFulLinka.Count() == 0)
            {
                useFulLink = null;
            }





            if (useFulLink == null)
            {
                return HttpNotFound();
            }
            return View(useFulLink);
        }

        // GET: Admin/UseFulLinks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/UseFulLinks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
        			public ActionResult Create([Bind(Include = "UseFulLinkId,Title,URL,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] UseFulLink useFulLink)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(useFulLink).State = EntityState.Added;
					useFulLink.CreatedBy = User.Identity.GetUserId();
                    useFulLink.CreatedDate = DateTime.Now;
        			    
				db.UseFullinks.Add(useFulLink);
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(useFulLink);
        }

        // GET: Admin/UseFulLinks/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UseFulLink useFulLink = db.UseFullinks.Find(id);
			var userid = User.Identity.GetUserId();
            var useFulLinka = from item in db.UseFullinks where (item.UseFulLinkId == id) select item;
            useFulLinka = useFulLinka.Include(a => a.CreatedBy);
            useFulLinka = useFulLinka.Where(a => a.CreatedBy == userid);
            if (useFulLinka.Count() == 0)
            {
                useFulLink = null;
            }
            if (useFulLink == null)
            {
                return HttpNotFound();
            }
            return View(useFulLink);
        }

        // POST: Admin/UseFulLinks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
					public ActionResult Edit([Bind(Include = "UseFulLinkId,Title,URL,CreatedDate,CreatedBy,DelFlg,Active")] UseFulLink useFulLink)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(useFulLink).State = EntityState.Modified;
				
				useFulLink.ModifiedBy = User.Identity.GetUserId();
                    useFulLink.ModifiedDate = DateTime.Now;
					                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(useFulLink);
        }

        // GET: Admin/UseFulLinks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UseFulLink useFulLink = db.UseFullinks.Find(id);
			var userid = User.Identity.GetUserId();
            var useFulLinka = from item in db.UseFullinks where (item.UseFulLinkId == id) select item;
            useFulLinka = useFulLinka.Include(a => a.CreatedBy);
            useFulLinka = useFulLinka.Where(a => a.CreatedBy == userid);
            if (useFulLinka.Count() == 0)
            {
                useFulLink = null;
            }

            if (useFulLink == null)
            {
                return HttpNotFound();
            }
            return View(useFulLink);
        }

        // POST: Admin/UseFulLinks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UseFulLink useFulLink = db.UseFullinks.Find(id);
            db.UseFullinks.Remove(useFulLink);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
