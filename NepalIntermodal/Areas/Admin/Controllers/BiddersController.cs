﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class BiddersController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Bidders
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingName = String.IsNullOrEmpty(sortingOrder) ? "Name" : "";
				            												ViewBag.SortingDescription = String.IsNullOrEmpty(sortingOrder) ? "Description" : "";
													            												ViewBag.SortingRepresentativeName = String.IsNullOrEmpty(sortingOrder) ? "RepresentativeName" : "";
				            												ViewBag.SortingContactNo = String.IsNullOrEmpty(sortingOrder) ? "ContactNo" : "";
				            												ViewBag.SortingRegestrationdate = String.IsNullOrEmpty(sortingOrder) ? "Regestrationdate" : "";
				            												ViewBag.SortingVatNo = String.IsNullOrEmpty(sortingOrder) ? "VatNo" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.Bidders select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            										
															item.Name.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Description.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.RepresentativeName.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.ContactNo.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
																				
													
															item.VatNo.ToUpper().Contains(searchData.ToUpper()));
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "Name":
											items = items.OrderByDescending(item => item.Name);
                    					break;
            			case "Description":
											items = items.OrderByDescending(item => item.Description);
                    					break;
            			case "RepresentativeName":
											items = items.OrderByDescending(item => item.RepresentativeName);
                    					break;
            			case "ContactNo":
											items = items.OrderByDescending(item => item.ContactNo);
                    					break;
            			case "Regestrationdate":
											items = items.OrderBy(item => item.Regestrationdate);	
										break;
            			case "VatNo":
											items = items.OrderByDescending(item => item.VatNo);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.Name);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/Bidders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Bidder bidder = db.Bidders.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var biddera = from item in db.Bidders where (item.BidderId == id) select item;
            biddera = biddera.Include(a => a.CreatedBy);
            biddera = biddera.Where(a => a.CreatedBy == userid);
            if (biddera.Count() == 0)
            {
                bidder = null;
            }





            if (bidder == null)
            {
                return HttpNotFound();
            }
            return View(bidder);
        }

        // GET: Admin/Bidders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Bidders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
        			public ActionResult Create([Bind(Include = "BidderId,Name,Description,RepresentativeName,ContactNo,Regestrationdate,VatNo,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] Bidder bidder)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(bidder).State = EntityState.Added;
					bidder.CreatedBy = User.Identity.GetUserId();
                    bidder.CreatedDate = DateTime.Now;
        			    
				db.Bidders.Add(bidder);
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bidder);
        }

        // GET: Admin/Bidders/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bidder bidder = db.Bidders.Find(id);
			var userid = User.Identity.GetUserId();
            var biddera = from item in db.Bidders where (item.BidderId == id) select item;
            biddera = biddera.Include(a => a.CreatedBy);
            biddera = biddera.Where(a => a.CreatedBy == userid);
            if (biddera.Count() == 0)
            {
                bidder = null;
            }
            if (bidder == null)
            {
                return HttpNotFound();
            }
            return View(bidder);
        }

        // POST: Admin/Bidders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
					public ActionResult Edit([Bind(Include = "BidderId,Name,Description,RepresentativeName,ContactNo,Regestrationdate,VatNo,CreatedDate,CreatedBy,DelFlg,Active")] Bidder bidder)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(bidder).State = EntityState.Modified;
				
				bidder.ModifiedBy = User.Identity.GetUserId();
                    bidder.ModifiedDate = DateTime.Now;
					                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bidder);
        }

        // GET: Admin/Bidders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bidder bidder = db.Bidders.Find(id);
			var userid = User.Identity.GetUserId();
            var biddera = from item in db.Bidders where (item.BidderId == id) select item;
            biddera = biddera.Include(a => a.CreatedBy);
            biddera = biddera.Where(a => a.CreatedBy == userid);
            if (biddera.Count() == 0)
            {
                bidder = null;
            }

            if (bidder == null)
            {
                return HttpNotFound();
            }
            return View(bidder);
        }

        // POST: Admin/Bidders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bidder bidder = db.Bidders.Find(id);
            db.Bidders.Remove(bidder);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
