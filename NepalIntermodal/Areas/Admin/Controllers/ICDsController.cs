﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class ICDsController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/ICDs
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingDescription = String.IsNullOrEmpty(sortingOrder) ? "Description" : "";
													            																					ViewBag.SortingImageUrl = String.IsNullOrEmpty(sortingOrder) ? "ImageUrl" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.ICDs select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            										
															item.Description.ToUpper().Contains(searchData.ToUpper()) );
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "Description":
											items = items.OrderByDescending(item => item.Description);
                    					break;
            			case "ImageUrl":
											items = items.OrderByDescending(item => item.ImageUrl);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.Description);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/ICDs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           ICD iCD = db.ICDs.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var iCDa = from item in db.ICDs where (item.ICDId == id) select item;
            iCDa = iCDa.Include(a => a.CreatedBy);
            iCDa = iCDa.Where(a => a.CreatedBy == userid);
            if (iCDa.Count() == 0)
            {
                iCD = null;
            }





            if (iCD == null)
            {
                return HttpNotFound();
            }
            return View(iCD);
        }

        // GET: Admin/ICDs/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/ICDs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
        			public ActionResult Create([Bind(Include = "ICDId,Description,ImageUrl,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] ICD iCD, HttpPostedFileBase ImageUrl)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(iCD).State = EntityState.Added;
					iCD.CreatedBy = User.Identity.GetUserId();
                    iCD.CreatedDate = DateTime.Now;
               
			 if (ImageUrl != null)
                {
                    string pathToCreate = "~/Images/iCD";
                    if (!Directory.Exists(Server.MapPath(pathToCreate)))
                    {
                        //Now you know it is ok, create it
                        Directory.CreateDirectory(Server.MapPath(pathToCreate));
                    }
                    string extension = Path.GetExtension(ImageUrl.FileName);
                    string fileName = Path.GetFileNameWithoutExtension(ImageUrl.FileName);
                    string id = Guid.NewGuid().ToString();
                    string imageUrl = "/Images/iCD/" + fileName + "" + /*main.MainId.ToString()*/ id + extension;

                    ImageUrl.SaveAs(Path.Combine(Server.MapPath(pathToCreate), fileName + "" + /*main.MainId.ToString()*/id + extension));
                    string DestinationPath = Path.Combine(Server.MapPath(pathToCreate));
                    DestinationPath += "\\" + fileName + id ;
					ImageResizer.ImageJob i = new ImageResizer.ImageJob(DestinationPath + extension , DestinationPath + "_thumb.jpg" , new ImageResizer.ResizeSettings(
                 "width=200;height=200;format=jpg;mode=max"));
					i.Build();
					
					iCD.ImageUrl = imageUrl;
	                    
						db.ICDs.Add(iCD);

                }
                else
                {
					db.ICDs.Add(iCD);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(iCD);
        }

        // GET: Admin/ICDs/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ICD iCD = db.ICDs.Find(id);
			var userid = User.Identity.GetUserId();
            var iCDa = from item in db.ICDs where (item.ICDId == id) select item;
            iCDa = iCDa.Include(a => a.CreatedBy);
            iCDa = iCDa.Where(a => a.CreatedBy == userid);
            if (iCDa.Count() == 0)
            {
                iCD = null;
            }
            if (iCD == null)
            {
                return HttpNotFound();
            }
            return View(iCD);
        }

        // POST: Admin/ICDs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
					public ActionResult Edit([Bind(Include = "ICDId,Description,ImageUrl,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] ICD iCD,HttpPostedFileBase ImageUrl)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(iCD).State = EntityState.Modified;
				
				iCD.ModifiedBy = User.Identity.GetUserId();
                    iCD.ModifiedDate = DateTime.Now;
				       
				if (ImageUrl != null)
                {
                    string pathToCreate = "~/Images/iCD";
                    if (!Directory.Exists(Server.MapPath(pathToCreate)))
                    {
                        //Now you know it is ok, create it
                        Directory.CreateDirectory(Server.MapPath(pathToCreate));
                    }
                    string extension = Path.GetExtension(ImageUrl.FileName);
                    string fileName = Path.GetFileNameWithoutExtension(ImageUrl.FileName);
                    string id = Guid.NewGuid().ToString();
                    string imageUrl = "/Images/iCD/" + fileName + "" + /*main.MainId.ToString()*/ id + extension;

                    ImageUrl.SaveAs(Path.Combine(Server.MapPath(pathToCreate), fileName + "" + /*main.MainId.ToString()*/id + extension));
					string DestinationPath = Path.Combine(Server.MapPath(pathToCreate));
                    DestinationPath += "\\" + fileName + id ;
					ImageResizer.ImageJob i = new ImageResizer.ImageJob(DestinationPath + extension , DestinationPath + "_thumb.jpg" , new ImageResizer.ResizeSettings(
					"width=200;height=200;format=jpg;mode=max"));
					i.Build();
					iCD.ImageUrl = imageUrl;
                }
				                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iCD);
        }

        // GET: Admin/ICDs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ICD iCD = db.ICDs.Find(id);
			var userid = User.Identity.GetUserId();
            var iCDa = from item in db.ICDs where (item.ICDId == id) select item;
            iCDa = iCDa.Include(a => a.CreatedBy);
            iCDa = iCDa.Where(a => a.CreatedBy == userid);
            if (iCDa.Count() == 0)
            {
                iCD = null;
            }

            if (iCD == null)
            {
                return HttpNotFound();
            }
            return View(iCD);
        }

        // POST: Admin/ICDs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ICD iCD = db.ICDs.Find(id);
            db.ICDs.Remove(iCD);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
