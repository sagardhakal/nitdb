﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class BillsController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/Bills
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingBillDescription = String.IsNullOrEmpty(sortingOrder) ? "BillDescription" : "";
				            												ViewBag.SortingTopic = String.IsNullOrEmpty(sortingOrder) ? "Topic" : "";
				            												ViewBag.SortingExpenseTopic = String.IsNullOrEmpty(sortingOrder) ? "ExpenseTopic" : "";
				            												ViewBag.SortingFromProcess = String.IsNullOrEmpty(sortingOrder) ? "FromProcess" : "";
				            												ViewBag.SortingPanNo = String.IsNullOrEmpty(sortingOrder) ? "PanNo" : "";
				            												ViewBag.SortingPaidTo = String.IsNullOrEmpty(sortingOrder) ? "PaidTo" : "";
				            												ViewBag.SortingReceivedDate = String.IsNullOrEmpty(sortingOrder) ? "ReceivedDate" : "";
				            												ViewBag.SortingPrice = String.IsNullOrEmpty(sortingOrder) ? "Price" : "";
				            												ViewBag.SortingRemarks = String.IsNullOrEmpty(sortingOrder) ? "Remarks" : "";
				            												ViewBag.SortingUploadTime = String.IsNullOrEmpty(sortingOrder) ? "UploadTime" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.Bills select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            										
															item.BillDescription.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Topic.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.ExpenseTopic.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.FromProcess.ToUpper().Contains(searchData.ToUpper()) ||
																
															item.PaidTo.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
																				
													
															item.Price.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Remarks.ToUpper().Contains(searchData.ToUpper()) );
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "BillDescription":
											items = items.OrderByDescending(item => item.BillDescription);
                    					break;
            			case "Topic":
											items = items.OrderByDescending(item => item.Topic);
                    					break;
            			case "ExpenseTopic":
											items = items.OrderByDescending(item => item.ExpenseTopic);
                    					break;
            			case "FromProcess":
											items = items.OrderByDescending(item => item.FromProcess);
                    					break;
            			case "PanNo":
											items = items.OrderByDescending(item => item.PanNo);
                    					break;
            			case "PaidTo":
											items = items.OrderByDescending(item => item.PaidTo);
                    					break;
            			case "ReceivedDate":
											items = items.OrderBy(item => item.ReceivedDate);	
										break;
            			case "Price":
											items = items.OrderByDescending(item => item.Price);
                    					break;
            			case "Remarks":
											items = items.OrderByDescending(item => item.Remarks);
                    					break;
            			case "UploadTime":
											items = items.OrderByDescending(item => item.UploadTime);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.BillDescription);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/Bills/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Bill bill = db.Bills.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var billa = from item in db.Bills where (item.BillId == id) select item;
            billa = billa.Include(a => a.CreatedBy);
            billa = billa.Where(a => a.CreatedBy == userid);
            if (billa.Count() == 0)
            {
                bill = null;
            }





            if (bill == null)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // GET: Admin/Bills/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Bills/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
        			public ActionResult Create([Bind(Include = "BillId,BillDescription,Topic,ExpenseTopic,FromProcess,PanNo,PaidTo,ReceivedDate,Price,Remarks,UploadTime,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] Bill bill)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(bill).State = EntityState.Added;
					bill.CreatedBy = User.Identity.GetUserId();
                    bill.CreatedDate = DateTime.Now;
        			    
				db.Bills.Add(bill);
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(bill);
        }

        // GET: Admin/Bills/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = db.Bills.Find(id);
			var userid = User.Identity.GetUserId();
            var billa = from item in db.Bills where (item.BillId == id) select item;
            billa = billa.Include(a => a.CreatedBy);
            billa = billa.Where(a => a.CreatedBy == userid);
            if (billa.Count() == 0)
            {
                bill = null;
            }
            if (bill == null)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // POST: Admin/Bills/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
					public ActionResult Edit([Bind(Include = "BillId,BillDescription,Topic,ExpenseTopic,FromProcess,PanNo,PaidTo,ReceivedDate,Price,Remarks,UploadTime,CreatedDate,CreatedBy,DelFlg,Active")] Bill bill)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(bill).State = EntityState.Modified;
				
				bill.ModifiedBy = User.Identity.GetUserId();
                    bill.ModifiedDate = DateTime.Now;
					                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(bill);
        }

        // GET: Admin/Bills/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bill bill = db.Bills.Find(id);
			var userid = User.Identity.GetUserId();
            var billa = from item in db.Bills where (item.BillId == id) select item;
            billa = billa.Include(a => a.CreatedBy);
            billa = billa.Where(a => a.CreatedBy == userid);
            if (billa.Count() == 0)
            {
                bill = null;
            }

            if (bill == null)
            {
                return HttpNotFound();
            }
            return View(bill);
        }

        // POST: Admin/Bills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Bill bill = db.Bills.Find(id);
            db.Bills.Remove(bill);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
