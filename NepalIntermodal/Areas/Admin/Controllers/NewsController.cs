﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class NewsController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/News
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingNewsId = String.IsNullOrEmpty(sortingOrder) ? "NewsId" : "";
				            												ViewBag.SortingTitle = String.IsNullOrEmpty(sortingOrder) ? "Title" : "";
				            												ViewBag.SortingDescription = String.IsNullOrEmpty(sortingOrder) ? "Description" : "";
													            												ViewBag.SortingPublishdate = String.IsNullOrEmpty(sortingOrder) ? "Publishdate" : "";
				            												ViewBag.SortingReference = String.IsNullOrEmpty(sortingOrder) ? "Reference" : "";
				            																					ViewBag.SortingLinkUrl = String.IsNullOrEmpty(sortingOrder) ? "LinkUrl" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.News select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            										
															item.Title.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Description.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
																				
													
															item.Reference.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.LinkUrl.ToUpper().Contains(searchData.ToUpper()) );
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "NewsId":
											items = items.OrderByDescending(item => item.NewsId);
                    					break;
            			case "Title":
											items = items.OrderByDescending(item => item.Title);
                    					break;
            			case "Description":
											items = items.OrderByDescending(item => item.Description);
                    					break;
            			case "Publishdate":
											items = items.OrderBy(item => item.Publishdate);	
										break;
            			case "Reference":
											items = items.OrderByDescending(item => item.Reference);
                    					break;
            			case "LinkUrl":
											items = items.OrderByDescending(item => item.LinkUrl);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.NewsId);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/News/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           News news = db.News.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var newsa = from item in db.News where (item.NewsId == id) select item;
            newsa = newsa.Include(a => a.CreatedBy);
            newsa = newsa.Where(a => a.CreatedBy == userid);
            if (newsa.Count() == 0)
            {
                news = null;
            }





            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // GET: Admin/News/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/News/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
        			public ActionResult Create([Bind(Include = "NewsId,Title,Description,Publishdate,Reference,LinkUrl,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] News news, HttpPostedFileBase LinkUrl)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(news).State = EntityState.Added;
					news.CreatedBy = User.Identity.GetUserId();
                    news.CreatedDate = DateTime.Now;
        			    
				db.News.Add(news);
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(news);
        }

        // GET: Admin/News/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
			var userid = User.Identity.GetUserId();
            var newsa = from item in db.News where (item.NewsId == id) select item;
            newsa = newsa.Include(a => a.CreatedBy);
            newsa = newsa.Where(a => a.CreatedBy == userid);
            if (newsa.Count() == 0)
            {
                news = null;
            }
            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // POST: Admin/News/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
				public ActionResult Edit([Bind(Include = "NewsId,Title,Description,Publishdate,Reference,LinkUrl,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] News news, HttpPostedFileBase LinkUrl)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(news).State = EntityState.Modified;
				
				news.ModifiedBy = User.Identity.GetUserId();
                    news.ModifiedDate = DateTime.Now;
					                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(news);
        }

        // GET: Admin/News/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            News news = db.News.Find(id);
			var userid = User.Identity.GetUserId();
            var newsa = from item in db.News where (item.NewsId == id) select item;
            newsa = newsa.Include(a => a.CreatedBy);
            newsa = newsa.Where(a => a.CreatedBy == userid);
            if (newsa.Count() == 0)
            {
                news = null;
            }

            if (news == null)
            {
                return HttpNotFound();
            }
            return View(news);
        }

        // POST: Admin/News/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            News news = db.News.Find(id);
            db.News.Remove(news);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
