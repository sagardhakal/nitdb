﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class BoardMembersController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/BoardMembers
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingName = String.IsNullOrEmpty(sortingOrder) ? "Name" : "";
				            												ViewBag.SortingEmail = String.IsNullOrEmpty(sortingOrder) ? "Email" : "";
				            												ViewBag.SortingPhone = String.IsNullOrEmpty(sortingOrder) ? "Phone" : "";
				            																					ViewBag.SortingImageUrl = String.IsNullOrEmpty(sortingOrder) ? "ImageUrl" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.BoardMembers select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            										
															item.Name.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Email.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Phone.ToUpper().Contains(searchData.ToUpper()) );
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "Name":
											items = items.OrderByDescending(item => item.Name);
                    					break;
            			case "Email":
											items = items.OrderByDescending(item => item.Email);
                    					break;
            			case "Phone":
											items = items.OrderByDescending(item => item.Phone);
                    					break;
            			case "ImageUrl":
											items = items.OrderByDescending(item => item.ImageUrl);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.Name);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/BoardMembers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           BoardMember boardMember = db.BoardMembers.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var boardMembera = from item in db.BoardMembers where (item.BoardMemberId == id) select item;
            boardMembera = boardMembera.Include(a => a.CreatedBy);
            boardMembera = boardMembera.Where(a => a.CreatedBy == userid);
            if (boardMembera.Count() == 0)
            {
                boardMember = null;
            }





            if (boardMember == null)
            {
                return HttpNotFound();
            }
            return View(boardMember);
        }

        // GET: Admin/BoardMembers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/BoardMembers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
        			public ActionResult Create([Bind(Include = "BoardMemberId,Name,Email,Phone,ImageUrl,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] BoardMember boardMember, HttpPostedFileBase ImageUrl)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(boardMember).State = EntityState.Added;
					boardMember.CreatedBy = User.Identity.GetUserId();
                    boardMember.CreatedDate = DateTime.Now;
               
			 if (ImageUrl != null)
                {
                    string pathToCreate = "~/Images/boardMember";
                    if (!Directory.Exists(Server.MapPath(pathToCreate)))
                    {
                        //Now you know it is ok, create it
                        Directory.CreateDirectory(Server.MapPath(pathToCreate));
                    }
                    string extension = Path.GetExtension(ImageUrl.FileName);
                    string fileName = Path.GetFileNameWithoutExtension(ImageUrl.FileName);
                    string id = Guid.NewGuid().ToString();
                    string imageUrl = "/Images/boardMember/" + fileName + "" + /*main.MainId.ToString()*/ id + extension;

                    ImageUrl.SaveAs(Path.Combine(Server.MapPath(pathToCreate), fileName + "" + /*main.MainId.ToString()*/id + extension));
                    string DestinationPath = Path.Combine(Server.MapPath(pathToCreate));
                    DestinationPath += "\\" + fileName + id ;
					ImageResizer.ImageJob i = new ImageResizer.ImageJob(DestinationPath + extension , DestinationPath + "_thumb.jpg" , new ImageResizer.ResizeSettings(
                 "width=200;height=200;format=jpg;mode=max"));
					i.Build();
					
					boardMember.ImageUrl = imageUrl;
	                    
						db.BoardMembers.Add(boardMember);

                }
                else
                {
					db.BoardMembers.Add(boardMember);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(boardMember);
        }

        // GET: Admin/BoardMembers/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BoardMember boardMember = db.BoardMembers.Find(id);
			var userid = User.Identity.GetUserId();
            var boardMembera = from item in db.BoardMembers where (item.BoardMemberId == id) select item;
            boardMembera = boardMembera.Include(a => a.CreatedBy);
            boardMembera = boardMembera.Where(a => a.CreatedBy == userid);
            if (boardMembera.Count() == 0)
            {
                boardMember = null;
            }
            if (boardMember == null)
            {
                return HttpNotFound();
            }
            return View(boardMember);
        }

        // POST: Admin/BoardMembers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
					public ActionResult Edit([Bind(Include = "BoardMemberId,Name,Email,Phone,ImageUrl,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] BoardMember boardMember,HttpPostedFileBase ImageUrl)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(boardMember).State = EntityState.Modified;
				
				boardMember.ModifiedBy = User.Identity.GetUserId();
                    boardMember.ModifiedDate = DateTime.Now;
				       
				if (ImageUrl != null)
                {
                    string pathToCreate = "~/Images/boardMember";
                    if (!Directory.Exists(Server.MapPath(pathToCreate)))
                    {
                        //Now you know it is ok, create it
                        Directory.CreateDirectory(Server.MapPath(pathToCreate));
                    }
                    string extension = Path.GetExtension(ImageUrl.FileName);
                    string fileName = Path.GetFileNameWithoutExtension(ImageUrl.FileName);
                    string id = Guid.NewGuid().ToString();
                    string imageUrl = "/Images/boardMember/" + fileName + "" + /*main.MainId.ToString()*/ id + extension;

                    ImageUrl.SaveAs(Path.Combine(Server.MapPath(pathToCreate), fileName + "" + /*main.MainId.ToString()*/id + extension));
					string DestinationPath = Path.Combine(Server.MapPath(pathToCreate));
                    DestinationPath += "\\" + fileName + id ;
					ImageResizer.ImageJob i = new ImageResizer.ImageJob(DestinationPath + extension , DestinationPath + "_thumb.jpg" , new ImageResizer.ResizeSettings(
					"width=200;height=200;format=jpg;mode=max"));
					i.Build();
					boardMember.ImageUrl = imageUrl;
                }
				                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(boardMember);
        }

        // GET: Admin/BoardMembers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BoardMember boardMember = db.BoardMembers.Find(id);
			var userid = User.Identity.GetUserId();
            var boardMembera = from item in db.BoardMembers where (item.BoardMemberId == id) select item;
            boardMembera = boardMembera.Include(a => a.CreatedBy);
            boardMembera = boardMembera.Where(a => a.CreatedBy == userid);
            if (boardMembera.Count() == 0)
            {
                boardMember = null;
            }

            if (boardMember == null)
            {
                return HttpNotFound();
            }
            return View(boardMember);
        }

        // POST: Admin/BoardMembers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BoardMember boardMember = db.BoardMembers.Find(id);
            db.BoardMembers.Remove(boardMember);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
