﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class LegalDocument_TypeController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/LegalDocument_Type
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingLegalDocument_TypeId = String.IsNullOrEmpty(sortingOrder) ? "LegalDocument_TypeId" : "";
				            												ViewBag.SortingName = String.IsNullOrEmpty(sortingOrder) ? "Name" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.LegalDocument_Type select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            										
															
													
															item.Name.ToUpper().Contains(searchData.ToUpper()) );
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "LegalDocument_TypeId":
											items = items.OrderByDescending(item => item.LegalDocument_TypeId);
                    					break;
            			case "Name":
											items = items.OrderByDescending(item => item.Name);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.LegalDocument_TypeId);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/LegalDocument_Type/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           LegalDocument_Type legalDocument_Type = db.LegalDocument_Type.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var legalDocument_Typea = from item in db.LegalDocument_Type where (item.LegalDocument_TypeId == id) select item;
            legalDocument_Typea = legalDocument_Typea.Include(a => a.CreatedBy);
            legalDocument_Typea = legalDocument_Typea.Where(a => a.CreatedBy == userid);
            if (legalDocument_Typea.Count() == 0)
            {
                legalDocument_Type = null;
            }





            if (legalDocument_Type == null)
            {
                return HttpNotFound();
            }
            return View(legalDocument_Type);
        }

        // GET: Admin/LegalDocument_Type/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/LegalDocument_Type/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
        			public ActionResult Create([Bind(Include = "LegalDocument_TypeId,Name,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] LegalDocument_Type legalDocument_Type)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(legalDocument_Type).State = EntityState.Added;
					legalDocument_Type.CreatedBy = User.Identity.GetUserId();
                    legalDocument_Type.CreatedDate = DateTime.Now;
        			    
				db.LegalDocument_Type.Add(legalDocument_Type);
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(legalDocument_Type);
        }

        // GET: Admin/LegalDocument_Type/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LegalDocument_Type legalDocument_Type = db.LegalDocument_Type.Find(id);
			var userid = User.Identity.GetUserId();
            var legalDocument_Typea = from item in db.LegalDocument_Type where (item.LegalDocument_TypeId == id) select item;
            legalDocument_Typea = legalDocument_Typea.Include(a => a.CreatedBy);
            legalDocument_Typea = legalDocument_Typea.Where(a => a.CreatedBy == userid);
            if (legalDocument_Typea.Count() == 0)
            {
                legalDocument_Type = null;
            }
            if (legalDocument_Type == null)
            {
                return HttpNotFound();
            }
            return View(legalDocument_Type);
        }

        // POST: Admin/LegalDocument_Type/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
		
					public ActionResult Edit([Bind(Include = "LegalDocument_TypeId,Name,CreatedDate,CreatedBy,DelFlg,Active")] LegalDocument_Type legalDocument_Type)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(legalDocument_Type).State = EntityState.Modified;
				
				legalDocument_Type.ModifiedBy = User.Identity.GetUserId();
                    legalDocument_Type.ModifiedDate = DateTime.Now;
					                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(legalDocument_Type);
        }

        // GET: Admin/LegalDocument_Type/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LegalDocument_Type legalDocument_Type = db.LegalDocument_Type.Find(id);
			var userid = User.Identity.GetUserId();
            var legalDocument_Typea = from item in db.LegalDocument_Type where (item.LegalDocument_TypeId == id) select item;
            legalDocument_Typea = legalDocument_Typea.Include(a => a.CreatedBy);
            legalDocument_Typea = legalDocument_Typea.Where(a => a.CreatedBy == userid);
            if (legalDocument_Typea.Count() == 0)
            {
                legalDocument_Type = null;
            }

            if (legalDocument_Type == null)
            {
                return HttpNotFound();
            }
            return View(legalDocument_Type);
        }

        // POST: Admin/LegalDocument_Type/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LegalDocument_Type legalDocument_Type = db.LegalDocument_Type.Find(id);
            db.LegalDocument_Type.Remove(legalDocument_Type);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
