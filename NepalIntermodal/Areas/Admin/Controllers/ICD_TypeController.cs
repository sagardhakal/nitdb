﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using NepalIntermodal.Areas.Admin.Models;
using NepalIntermodal.Models;

namespace NepalIntermodal.Areas.Admin.Controllers

{
	[Authorize]
    public class ICD_TypeController : Controller
    
	{
		
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Admin/ICD_Type
        public ActionResult Index(string sortingOrder, string searchData, string filterValue, int? pageNo)
        {
            ViewBag.CurrentSortOrder = sortingOrder;
            
															ViewBag.SortingICD_TypeId = String.IsNullOrEmpty(sortingOrder) ? "ICD_TypeId" : "";
				            												ViewBag.SortingName = String.IsNullOrEmpty(sortingOrder) ? "Name" : "";
				            												ViewBag.SortingDescription = String.IsNullOrEmpty(sortingOrder) ? "Description" : "";
													            																					ViewBag.SortingImageUrl = String.IsNullOrEmpty(sortingOrder) ? "ImageUrl" : "";
				            												ViewBag.SortingActive = String.IsNullOrEmpty(sortingOrder) ? "Active" : "";
				            			
			
			var items = from item in db.ICD_Type select item;
			if ((searchData != null && searchData.ToString() != "")|| (filterValue !=null && filterValue.ToString() != ""))
			{
			if (filterValue != null)
                {
                    searchData = filterValue;}
                else
                {
                    pageNo = 1;
                }
				 items =
                items.Where(
                    item =>
            															
													
															item.Name.ToUpper().Contains(searchData.ToUpper()) ||
																				
													
															item.Description.ToUpper().Contains(searchData.ToUpper()));
																					
						}
			ViewBag.FilterValue = searchData;
			 switch (sortingOrder)
            {
						case "ICD_TypeId":
											items = items.OrderByDescending(item => item.ICD_TypeId);
                    					break;
            			case "Name":
											items = items.OrderByDescending(item => item.Name);
                    					break;
            			case "Description":
											items = items.OrderByDescending(item => item.Description);
                    					break;
            			case "ImageUrl":
											items = items.OrderByDescending(item => item.ImageUrl);
                    					break;
            			case "Active":
											items = items.OrderByDescending(item => item.Active);
                    					break;
            			default:
                    items = items.OrderBy(item => item.ICD_TypeId);
                    break;
			}
			const int totalPageSize = 10;
			var noOfPage = (pageNo ?? 1);
			return View(items.ToPagedList(noOfPage ,totalPageSize ));
        }

        // GET: Admin/ICD_Type/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           ICD_Type iCD_Type = db.ICD_Type.Find(id);
			//Below Lines added for Community
            var userid = User.Identity.GetUserId();
            var iCD_Typea = from item in db.ICD_Type where (item.ICD_TypeId == id) select item;
            iCD_Typea = iCD_Typea.Include(a => a.CreatedBy);
            iCD_Typea = iCD_Typea.Where(a => a.CreatedBy == userid);
            if (iCD_Typea.Count() == 0)
            {
                iCD_Type = null;
            }





            if (iCD_Type == null)
            {
                return HttpNotFound();
            }
            return View(iCD_Type);
        }

        // GET: Admin/ICD_Type/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/ICD_Type/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
        			public ActionResult Create([Bind(Include = "ICD_TypeId,Name,Description,ImageUrl,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] ICD_Type iCD_Type, HttpPostedFileBase ImageUrl)
		
        {
            if (ModelState.IsValid)
            {
					db.Entry(iCD_Type).State = EntityState.Added;
					iCD_Type.CreatedBy = User.Identity.GetUserId();
                    iCD_Type.CreatedDate = DateTime.Now;
               
			 if (ImageUrl != null)
                {
                    string pathToCreate = "~/Images/iCD_Type";
                    if (!Directory.Exists(Server.MapPath(pathToCreate)))
                    {
                        //Now you know it is ok, create it
                        Directory.CreateDirectory(Server.MapPath(pathToCreate));
                    }
                    string extension = Path.GetExtension(ImageUrl.FileName);
                    string fileName = Path.GetFileNameWithoutExtension(ImageUrl.FileName);
                    string id = Guid.NewGuid().ToString();
                    string imageUrl = "/Images/iCD_Type/" + fileName + "" + /*main.MainId.ToString()*/ id + extension;

                    ImageUrl.SaveAs(Path.Combine(Server.MapPath(pathToCreate), fileName + "" + /*main.MainId.ToString()*/id + extension));
                    string DestinationPath = Path.Combine(Server.MapPath(pathToCreate));
                    DestinationPath += "\\" + fileName + id ;
					ImageResizer.ImageJob i = new ImageResizer.ImageJob(DestinationPath + extension , DestinationPath + "_thumb.jpg" , new ImageResizer.ResizeSettings(
                 "width=200;height=200;format=jpg;mode=max"));
					i.Build();
					
					iCD_Type.ImageUrl = imageUrl;
	                    
						db.ICD_Type.Add(iCD_Type);

                }
                else
                {
					db.ICD_Type.Add(iCD_Type);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
		
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(iCD_Type);
        }

        // GET: Admin/ICD_Type/Edit/5
        
		public ActionResult Edit(int? id)

        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ICD_Type iCD_Type = db.ICD_Type.Find(id);
			var userid = User.Identity.GetUserId();
            var iCD_Typea = from item in db.ICD_Type where (item.ICD_TypeId == id) select item;
            iCD_Typea = iCD_Typea.Include(a => a.CreatedBy);
            iCD_Typea = iCD_Typea.Where(a => a.CreatedBy == userid);
            if (iCD_Typea.Count() == 0)
            {
                iCD_Type = null;
            }
            if (iCD_Type == null)
            {
                return HttpNotFound();
            }
            return View(iCD_Type);
        }

        // POST: Admin/ICD_Type/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
					[ValidateInput(false)] 
		
					public ActionResult Edit([Bind(Include = "ICD_TypeId,Name,Description,ImageUrl,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] ICD_Type iCD_Type,HttpPostedFileBase ImageUrl)
		
		
        {
            if (ModelState.IsValid)
            {
                db.Entry(iCD_Type).State = EntityState.Modified;
				
				iCD_Type.ModifiedBy = User.Identity.GetUserId();
                    iCD_Type.ModifiedDate = DateTime.Now;
				       
				if (ImageUrl != null)
                {
                    string pathToCreate = "~/Images/iCD_Type";
                    if (!Directory.Exists(Server.MapPath(pathToCreate)))
                    {
                        //Now you know it is ok, create it
                        Directory.CreateDirectory(Server.MapPath(pathToCreate));
                    }
                    string extension = Path.GetExtension(ImageUrl.FileName);
                    string fileName = Path.GetFileNameWithoutExtension(ImageUrl.FileName);
                    string id = Guid.NewGuid().ToString();
                    string imageUrl = "/Images/iCD_Type/" + fileName + "" + /*main.MainId.ToString()*/ id + extension;

                    ImageUrl.SaveAs(Path.Combine(Server.MapPath(pathToCreate), fileName + "" + /*main.MainId.ToString()*/id + extension));
					string DestinationPath = Path.Combine(Server.MapPath(pathToCreate));
                    DestinationPath += "\\" + fileName + id ;
					ImageResizer.ImageJob i = new ImageResizer.ImageJob(DestinationPath + extension , DestinationPath + "_thumb.jpg" , new ImageResizer.ResizeSettings(
					"width=200;height=200;format=jpg;mode=max"));
					i.Build();
					iCD_Type.ImageUrl = imageUrl;
                }
				                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(iCD_Type);
        }

        // GET: Admin/ICD_Type/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ICD_Type iCD_Type = db.ICD_Type.Find(id);
			var userid = User.Identity.GetUserId();
            var iCD_Typea = from item in db.ICD_Type where (item.ICD_TypeId == id) select item;
            iCD_Typea = iCD_Typea.Include(a => a.CreatedBy);
            iCD_Typea = iCD_Typea.Where(a => a.CreatedBy == userid);
            if (iCD_Typea.Count() == 0)
            {
                iCD_Type = null;
            }

            if (iCD_Type == null)
            {
                return HttpNotFound();
            }
            return View(iCD_Type);
        }

        // POST: Admin/ICD_Type/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ICD_Type iCD_Type = db.ICD_Type.Find(id);
            db.ICD_Type.Remove(iCD_Type);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
