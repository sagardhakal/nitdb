﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NepalIntermodal.Startup))]
namespace NepalIntermodal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
