﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NepalIntermodal.Models;
using NepalIntermodal.Areas.Admin.Models;
using System.Data.Entity;

namespace NepalIntermodal.Controllers
{
    
    public class HomeController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult BoardMembers()
        {
            return View();
        }
        public ActionResult OrganizationalStructure()
        {
            return View();
        }
        public ActionResult ICD()
        {
            return View();
        }

        public ActionResult Gallery()
        {
            return View();
        }
        public ActionResult Introduction()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            ViewBag.Message = "Your About page.";
            return View(db.Abouts.SingleOrDefault());
        }
         public ActionResult LegalDocuments(int? id)
         {
             ViewBag.Message = "Your Documents page.";
             var doctype = db.LegalDocuments.Where(x => x.LegalDocument_TypeId == id).Select(x => x.LegalDocument_Type.Name).SingleOrDefault();
             ViewBag.doctype = doctype;
             var docs = db.LegalDocuments.Where(x => x.LegalDocument_TypeId == id).ToList();
             return View(docs);
         }
         public ActionResult Publication(string id)
         {
             ViewBag.Message = "Your Publications page.";
             //var doctype = db.LegalDocuments.Where(x => x.LegalDocument_TypeId == id).Select(x => x.LegalDocument_Type.Name).SingleOrDefault();
             //ViewBag.doctype = doctype;

             
             var publication = db.Publications.Where(x => x.Publication_Type.ToString() == id).ToList();
             ViewBag.type = id;
             return View(publication);
         }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Contact([Bind(Include = "ContactId,Name,Email,PhoneNo,Message,CreatedDate,CreatedBy,ModifiedDate,ModifiedBy,DelFlg,Active")] Contact contact)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contact).State = EntityState.Added;
                contact.CreatedBy = "88be7b3e-c9e0-4785-8d77-14c58c431770";
                contact.CreatedDate = DateTime.Now;

                db.Contacts.Add(contact);

                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(contact);
        }
        public ActionResult NewsDetails(int? id)
        {
            ViewBag.Message = "Your Details page.";
            var newsdetails = db.News.Find(id);
            return View(newsdetails);
        }
        public ActionResult ActivityDetails(int? id)
        {
            ViewBag.Message = "Your Details page.";
            var activity = db.CurrentActivities.Find(id);
            return View(activity);
        }
    }
}